package com.mecatronica.common;

import java.io.Serializable;

public class AuthSession implements Serializable{
    private int user_id;
    private String user_key;
    private String user_pass;
    private String controller_id;

    public AuthSession (int user_id, String user_key, String user_pass, String controller_id){
        this.user_id = user_id;
        this.user_key = user_key;
        this.user_pass = user_pass;
        this.controller_id = controller_id;
    }

    public int getUser_id() {
        return user_id;
    }
    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getUser_key() {
        return user_key;
    }
    public void setUser_key(String user_key) {
        this.user_key = user_key;
    }

    public String getUser_pass() {
        return user_pass;
    }
    public void setUser_pass(String user_pass) {
        this.user_pass = user_pass;
    }

    public String getController_id() {
        return controller_id;
    }
    public void setController_id(String controller_id) {
        this.controller_id = controller_id;
    }
}