package com.mecatronica.common;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * Objeto basico que utilizaremos para transmitir, tanto en Wifi, como con Bluetooth.
 * Created by Javier on 20/11/14.
 */
public class NetworkObject implements Serializable{

    public static final int TAM_MINIPACKETS = 20;
    /**
     * Identificador de la funcion a realizar por el objeto.
     */
    private NetworkFunction idFunction;

    /**
     * Objeto que sera utilizado por la funcion indicada.
     */
    private Object object;

    public NetworkFunction getIdFunction() {
        return idFunction;
    }

    public void setIdFunction(NetworkFunction idFunction) {
        this.idFunction = idFunction;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    /**
     * Convierte el objeto en bytes.
     * @return bytes que representan al objeto.
     */
    public byte [] convertToBytes() {

        ByteArrayOutputStream bs = new ByteArrayOutputStream();
        try {
            ObjectOutputStream os = new ObjectOutputStream(bs);
            os.writeObject(this);
            os.close();
        }
        catch (IOException e) { }

        return bs.toByteArray();
    }

    /**
     * Convierte un grupo de bytes en un objeto.
     * @param bytes bytes del objeto serializado.
     * @return el objeto serialiazdo.
     */
    public static NetworkObject bytesToObject(byte [] bytes) {
        ByteArrayInputStream bs= new ByteArrayInputStream(bytes); // bytes es el byte[]
        NetworkObject objectRecieve = new NetworkObject();
        try {
            ObjectInputStream is = new ObjectInputStream(bs);
            objectRecieve = (NetworkObject) is.readObject();
            is.close();
        }
        catch (ClassNotFoundException e) { }
        catch (IOException e) { }

        return objectRecieve;
    }

    public static int getNumPackets(byte [] bytes) {
        int numPackets = bytes.length/ TAM_MINIPACKETS;

        if ((bytes.length % TAM_MINIPACKETS) != 0)
            numPackets++;

        return numPackets;
    }

}
