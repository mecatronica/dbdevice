package com.mecatronica.common;

import com.mecatronica.dbdevice.basic.iBeaconDetection;

/**
 * Clase con los datos de acceso para un punto de acceso determinado, para enviarlos al servidor
 * durante la autenticacion.
 *
 * Para obtener las direcciones de los controladores registrados, usar loadRegisteredAccessPointAddresses().
 * Para obtener los datos de un punto de acceso determinado, usar la funcin esttica loadAccessPointData().
 * Para guardar un fichero de configuracion en la aplicacion, usar saveAccessPointData().
 *
 * @author Juanjo
 *
 */
public class AccessPointData {

    //VARIABLES Y GETTERS

    private String accessPointAddress;

    /**Direccin del punto de acceso para la que es valido este AccessPointData.*/
    public String getAccessPointAddress() {
        return accessPointAddress;
    }

    private String accessPointName;

    /**Nombre del punto de acceso para la que es valido este AccessPointData.*/
    public String getAccessPointName() {
        return accessPointName;
    }

    private int userId;

    /**Identificador del usuario en el punto de acceso.*/
    public int getUserId() {
        return userId;
    }

    private String key;

    /**Llave con la que se valida al usuario en el punto de acceso.*/
    public String getKey() {
        return key;
    }

    //METODOS ESTATICOS

    /**
     *	Comprueba si la aplicacin tiene guardada datos de acceso para el punto de acceso indicado.
     *
     *	@param accessPointAddress La MAC del punto de acceso del que se quiere saber si existen datos de acceso.
     */
    public static boolean existsData(String accessPointAddress){
        return true;
    }

    /**
     *	Devuelve un listado con las direcciones de todos los puntos de acceso que se tienen registrados, es
     *	decir, de los puntos de acceso de los que se tienen datos de acceso.
     *
     * 	Las direcciones devueltas por esta clase se usaran como filtros en la clase iBeaconDetection, para
     * 	detectar solo los puntos de acceso de los que se tengan datos de acceso, y para obtener los datos de
     * 	un punto de acceso especifico en la funcion loadAccessPointData().
     */
    public static String[] loadRegisteredAccessPointAddresses(){
        // TODO: Solo disponemos de un dispositivo de momento
        return new String [] { iBeaconDetection.MAC_BLE };
    }

    /**
     *	Carga los datos de acceso para el controlador con la direccin indicada.
     *
     *	@param accessPointAddress La MAC del punto de acceso del que se quieren obtener los datos de acceso.
     *
     *	@return los datos de acceso para el punto de acceso indicado, o null si no hay datos guardados para ese
     *	punto de acceso, en cuyo caso debern cargarse estos datos antes usando la funcin saveAccessPointData().
     */
    public static AccessPointData loadAccessPointData(String accessPointAddress){
        // TODO: Codigo hardcodeado por que samu no ha hecho su parte.
        AccessPointData access = new AccessPointData();
        access.userId = 1;
        access.accessPointAddress = iBeaconDetection.MAC_BLE;
        access.accessPointName = "Mecatronica";
        access.key = "123456789";

        return access;
    }

    /**
     *	Carga los datos de un fichero de configuracin en la aplicacin.
     *
     *	Cada fichero de configuracin debe estar estructurado siguiendo la especificacin para el fichero de configuracin
     *	del dispositivo.
     *
     *	@param configurationFile El fichero de configuracin a usar, en forma de String.
     *
     *	@return true, si se consiguen guardar los datos del fichero en la memoria de la aplicacin, false si no
     *	se pueden guardar los datos (por ejemplo, porque uno de los AccessData del JSON son para un
     *	controlador que ya tiene datos registrados.
     */
    public static boolean saveAccessPointData(String configurationFile){
        return false;
    }

}