package com.mecatronica.common;

/**
 * Enum con las funciones del dispositivo, controlador, servidor y aplicacin de prueba a las que se puede llamar remotamente.
 * */
public enum NetworkFunction {
	
	/*TODO: Decir en un comentario a que funcin del destino corresponde cada enum.
	 * O, para ms claridad, hacer un enum de strings, y que el string del enum sea el nombre de su funcin asociada.
	 */

    DBSERVER_AUTHENTICATE_USER,

    DBCONTROLLER_SEND_USER_DATA,

    DBDEVICE_SERVER_RESPONSE,

    DBDEBUGAPP_WIFI_TEST;
}