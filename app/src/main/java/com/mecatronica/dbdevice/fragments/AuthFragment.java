package com.mecatronica.dbdevice.fragments;

import android.app.Fragment;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.mecatronica.dbdevice.R;
import com.mecatronica.dbdevice.activities.AuthActivity;
import com.mecatronica.dbdevice.adapters.iBeaconAdapter;
import com.mecatronica.dbdevice.basic.LoadBeacons;


/**
 * Clase encargada de "dibujar" la interfaz de la autentificacion.
 * Created by Javier on 30/11/14.
 */
public class AuthFragment extends Fragment {

    private TextView title;
    public static ListView listiBeacons;

    public static final String EXTRA_DEVICE_ADDRESS = "BEACON_ADDRESS";

    public static final int UPDATE_LIST_IBEACONS = 0;

    public static Handler updaterHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case UPDATE_LIST_IBEACONS:
                    // Update adapter of the ListView
                    ((iBeaconAdapter)listiBeacons.getAdapter()).notifyDataSetChanged();
                    break;
            }
            super.handleMessage(msg);
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_authentication, container, false);

        title = (TextView) view.findViewById(R.id.auth_textview);
        listiBeacons = (ListView) view.findViewById(R.id.listdevices_listview);
        /*final FloatingActionButton refreshButton = (FloatingActionButton) view.findViewById(R.id.fab);
        refreshButton.attachToListView(listiBeacons);

        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AnimatorSet animator = new AnimatorSet();
                ValueAnimator rotateButton = ObjectAnimator.ofFloat(refreshButton, "rotation", 0, 360);
                rotateButton.setDuration(1200).setRepeatCount(1);

                // Añadimos la rotacion al AnimatorSet y empezamos la animación.
                animator.play(rotateButton);
                animator.start();

                new LoadBeacons(getActivity(), listiBeacons).execute();
            }
        });
*/
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if(bluetoothAdapter.isEnabled())
            new LoadBeacons(getActivity(), listiBeacons).execute();

        listiBeacons.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                /* Cuando cambiemos de Activity, aqui deberemos pasar los datos que necesite la
                siguiente Activity para funcionar, en este caso, deberiamos pasarle el iBeacon
                para que AuthActivity, se pueda conectar por bluetooth al controlador y enviar el
                pin correspondiente.
                */
                Intent intent = new Intent(getActivity(), AuthActivity.class);
                TextView editText = (TextView) view.findViewById(R.id.list_ibeacon_uuid);
                String addr = editText.getText().toString();
                intent.putExtra(EXTRA_DEVICE_ADDRESS, addr);
                startActivity(intent);
            }
        });

        title.setText(R.string.title_auth);

        return view;
    }

}
