package com.mecatronica.dbdevice.fragments;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.mecatronica.dbdevice.R;
import com.mecatronica.dbdevice.activities.AuthActivity;
import com.mecatronica.dbdevice.basic.LoadBeacons;
import com.melnykov.fab.FloatingActionButton;

/**
 * Clase encargada de "pintar" la interfaz de admin.
 * Created by Javier on 30/11/14.
 */
public class AdminFragment extends Fragment{

    private TextView title;
    private ListView listiBeacons;

    public static final String EXTRA_DEVICE_ADMIN = "DISPOSITIVO_ADMIN";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_admin, container, false);

        title = (TextView)view.findViewById(R.id.admin_textview);
        listiBeacons = (ListView) view.findViewById(R.id.listadmindevices_listview);
        final FloatingActionButton refreshButton = (FloatingActionButton) view.findViewById(R.id.fab_admin);
        refreshButton.attachToListView(listiBeacons);

        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AnimatorSet animator = new AnimatorSet();
                ValueAnimator rotateButton = ObjectAnimator.ofFloat(refreshButton, "rotation", 0, 360);
                rotateButton.setDuration(1200).setRepeatCount(1);

                // Añadimos la rotacion al AnimatorSet y empezamos la animación.
                animator.play(rotateButton);
                animator.start();

                new LoadBeacons(getActivity(), listiBeacons).execute();
            }
        });

        listiBeacons.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                /* Cuando cambiemos de Activity, aqui deberemos pasar los datos que necesite la
                siguiente Activity para funcionar, en este caso, deberiamos pasarle el iBeacon
                para que AuthActivity, se pueda conectar por bluetooth al controlador y enviar el
                pin correspondiente.
                */
                Intent intent = new Intent(getActivity(), AuthActivity.class);
                TextView editText = (TextView) view.findViewById(R.id.list_ibeacon_uuid);
                String uuid = editText.getText().toString();
                intent.putExtra(EXTRA_DEVICE_ADMIN, uuid);
                startActivity(intent);
            }
        });
        title.setText(R.string.title_admin);

        //new LoadBeacons(getActivity(), listiBeacons).execute();

        return view;
    }
}
