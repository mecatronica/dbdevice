package com.mecatronica.dbdevice.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mecatronica.dbdevice.R;
import com.mecatronica.dbdevice.basic.iBeacon;

import java.util.List;

/**
 * Created by Javier on 27/11/14.
 */
public class iBeaconAdapter extends ArrayAdapter<iBeacon>{

    private static final int resource = R.layout.list_item_ibeacon;
    private Context context;
    private List<iBeacon> values;

    public iBeaconAdapter(Context context, List<iBeacon> values) {
        super(context, resource, values);
        this.values = values;
        this.context = context;
    }

    @Override
    public int getCount() {
        return values.size();
    }

    @Override
    public iBeacon getItem(int position) {
        return values.get(position);
    }

    public List<iBeacon> getItems() {
        return values;
    }

    public void setItems(List<iBeacon> values) {
        this.values = values;
    }

    public void setItem(iBeacon value) {
        this.values.add(value);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(resource, parent, false);

        TextView iBeaconUuid = (TextView) rowView.findViewById(R.id.list_ibeacon_uuid);
        TextView iBeaconDistance = (TextView) rowView.findViewById(R.id.list_ibeacon_distance);

        iBeacon item = getItem(position);

        iBeaconUuid.setText(item.getAddress());
        iBeaconDistance.setText(Float.toString(item.getPower()));

        return rowView;
    }
}
