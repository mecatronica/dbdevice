package com.mecatronica.dbdevice.activities;

import android.app.Activity;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mecatronica.common.AccessPointData;
import com.mecatronica.common.AuthSession;
import com.mecatronica.common.NetworkFunction;
import com.mecatronica.common.NetworkObject;
import com.mecatronica.dbdevice.BLEFramework.RBLService;
import com.mecatronica.dbdevice.R;
import com.mecatronica.dbdevice.fragments.AuthFragment;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class AuthActivity extends Activity {

    private TextView title;
    private EditText editPin;
    private Button sendPin;
    private RBLService bluetoothLeService;
    private String deviceAddress;
    private Map<UUID, BluetoothGattCharacteristic> map = new HashMap<UUID, BluetoothGattCharacteristic>();

    // Indica que el arduino esta preparado para recibir datos.
    private final int ACK_ARD = 1;
    // Indica que se ha terminado de enviar los datos y arduino los ha recibido
    private final int END_TRANS_ARD = 2;
    // Indica que la puerta se puede abrir
    private final int OPEN_DOOR = 3;
    // Indica que la puerta no se puede abrir
    private final int NOT_OPEN_DOOR = 4;

    private NetworkObject packet;
    private int index = 0;
    private final ServiceConnection serviceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName,
                                       IBinder service) {
            bluetoothLeService = ((RBLService.LocalBinder) service).getService();
            if (!bluetoothLeService.initialize()) {
                Log.e("BLUETOOTH", "Unable to initialize Bluetooth");
                finish();
            }
            // Automatically connects to the device upon successful start-up
            // initialization.
            bluetoothLeService.connect(deviceAddress);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            bluetoothLeService = null;
        }
    };

    private final BroadcastReceiver gattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            if (RBLService.ACTION_GATT_CONNECTED.equals(action)) {
                sendPin.setEnabled(true);
            }
            else if (RBLService.ACTION_GATT_DISCONNECTED.equals(action)) {
                sendPin.setEnabled(false);
            } else if (RBLService.ACTION_GATT_SERVICES_DISCOVERED
                    .equals(action)) {
                getGattService(bluetoothLeService.getSupportedGattService());
            } else if (RBLService.ACTION_DATA_AVAILABLE.equals(action)) {
                byte [] data = intent.getByteArrayExtra(RBLService.EXTRA_DATA);

                if (data != null) {
                    int ack = Integer.valueOf(new String(data));

                    // Si recibimos ACK_ARD, significa que despues de los datos enviados, arduino ya esta disponible
                    // para recibir mas datos.
                    if (ack == ACK_ARD) {
                        BluetoothGattCharacteristic characteristic = map.get(RBLService.UUID_BLE_SHIELD_TX);
                        byte [] bytesPacket = packet.convertToBytes();

                        Log.i("SERIAL", new String(packet.convertToBytes()));
                        byte [] miniPacket = new byte[NetworkObject.TAM_MINIPACKETS];

                        for(int j = 0;j < NetworkObject.TAM_MINIPACKETS && index < bytesPacket.length; j++, index++) {
                            miniPacket[j] = bytesPacket[index];
                            Log.i("PACKET", index + " " + miniPacket[j]);
                        }

                        characteristic.setValue(miniPacket);
                        bluetoothLeService.writeCharacteristic(characteristic);
                    }
                    else if (ack == END_TRANS_ARD)
                    {
                        // Cuando recibamos esto, significa que hemos enviado ttodo el paquete y Arduino
                        // lo ha recibidio correctamente. En principio, no deberiamos hacer nada aqui.
                        // Algo visual a lo sumo.
                    }
                    else if (ack == OPEN_DOOR)
                    {
                        sendPin.setEnabled(false);
                        editPin.setEnabled(false);
                        Toast.makeText(getApplicationContext(), "ABIERTA", Toast.LENGTH_LONG).show();
                    }
                    else if (ack == NOT_OPEN_DOOR)
                    {
                        Toast.makeText(getApplicationContext(), "INCORRECTO", Toast.LENGTH_LONG).show();
                    }

                }
                Log.i("RECIBIDO", new String(intent.getByteArrayExtra(RBLService.EXTRA_DATA)));
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        title = (TextView) findViewById(R.id.title_auth);
        sendPin = (Button) findViewById(R.id.button_send_pin);
        editPin = (EditText) findViewById(R.id.edit_pin);

        sendPin.setEnabled(false);
        title.setText(title.getText() + " " + getIntent().getStringExtra(AuthFragment.EXTRA_DEVICE_ADDRESS));


        Intent intent = getIntent();

        deviceAddress = intent.getStringExtra(AuthFragment.EXTRA_DEVICE_ADDRESS);

        Intent gattServiceIntent = new Intent(this, RBLService.class);
        bindService(gattServiceIntent, serviceConnection, BIND_AUTO_CREATE);

        sendPin.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                BluetoothGattCharacteristic characteristic = map
                        .get(RBLService.UUID_BLE_SHIELD_TX);

                String pin = editPin.getText().toString();
                packet = new NetworkObject();

                // Reseteamos el indice.
                index = 0;

                // Cogemos los datos del supuesto fichero de configuracion.
                AccessPointData access = AccessPointData.loadAccessPointData(deviceAddress);
                int user_id = access.getUserId();
                String key = access.getKey();
                AuthSession auth = new AuthSession(user_id, key, pin, new String());
                // Indicamos que funcion y que objeto ha de tener el paquete.
                packet.setIdFunction(NetworkFunction.DBSERVER_AUTHENTICATE_USER);
                packet.setObject(auth);


                // Convertimos el paquete a bytes.
                byte [] bytesPacket = packet.convertToBytes();

                // Enviamos el tamaño del paquete.
                characteristic.setValue(bytesPacket.length,BluetoothGattCharacteristic.FORMAT_SINT16,0);
                bluetoothLeService.writeCharacteristic(characteristic);

               /* Log.i("NUMPACKETS", String.valueOf(bytesPacket.length) + " " + Short.SIZE);
                Log.i("NUMPACKETS", String.valueOf(com.mecatronica.common.NetworkObject.getNumPackets(bytesPacket)));*/

                title.setText(new String(pin));
                editPin.setText("");
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        registerReceiver(gattUpdateReceiver, makeGattUpdateIntentFilter());
    }

    @Override
    protected void onStop() {
        super.onStop();

        unregisterReceiver(gattUpdateReceiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Cerramos la conexion.
        bluetoothLeService.disconnect();
        bluetoothLeService.close();

        unbindService(serviceConnection);
    }

    private void getGattService(BluetoothGattService gattService) {
        if (gattService == null)
            return;

        BluetoothGattCharacteristic characteristic = gattService
                .getCharacteristic(RBLService.UUID_BLE_SHIELD_TX);
        map.put(characteristic.getUuid(), characteristic);

        BluetoothGattCharacteristic characteristicRx = gattService
                .getCharacteristic(RBLService.UUID_BLE_SHIELD_RX);
        bluetoothLeService.setCharacteristicNotification(characteristicRx,
                true);
        bluetoothLeService.readCharacteristic(characteristicRx);
    }

    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();

        intentFilter.addAction(RBLService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(RBLService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(RBLService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(RBLService.ACTION_DATA_AVAILABLE);

        return intentFilter;
    }

}