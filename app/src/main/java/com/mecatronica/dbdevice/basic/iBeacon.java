package com.mecatronica.dbdevice.basic;

/**
 * Created by Javier on 27/11/14.
 */
public class iBeacon {

	/**Identificador del iBeacon.*/
    private String address;

    /** Nombre del iBeacon */
    private String name;

    /**Potencia de transmision del iBeacon. Cuanta mayor sea la potencia, mas cerca se esta del iBeacon.*/
    private int power;


    public iBeacon(String name, String address, int power) {
        this.name = name;
        this.address = address;
        this.power = power;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public float getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
