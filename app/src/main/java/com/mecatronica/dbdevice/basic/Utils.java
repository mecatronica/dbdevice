package com.mecatronica.dbdevice.basic;

/**
 * Created by Javier on 16/11/14.
 * Aqui pondremos los enums y clases globales que necesitemos
 */

/**
 * Representa el tipo de usuario que podemos ser, ADMIN, o USER.
 */
enum UserType {
        ADMIN,
        USER
}

