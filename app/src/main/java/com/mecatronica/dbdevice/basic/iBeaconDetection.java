package com.mecatronica.dbdevice.basic;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;

import com.mecatronica.common.AccessPointData;
import com.mecatronica.dbdevice.adapters.iBeaconAdapter;
import com.mecatronica.dbdevice.fragments.AuthFragment;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Clase encargada de detectar los iBeacons cercanos, y de comprobar si alguno de 
 * esos iBeacons es uno de los que se est� buscando.
 */
public class iBeaconDetection extends ScanCallback {
	
	/**Identificadores de los puntos de acceso a los que el dispositivo se puede conectar.*/
	private ArrayList<UUID> iBeaconIdentifiers;
	
	/**Clase de Android encargada de la detecci�n de iBeacons.*/
	private BluetoothLeScanner scanner;
	
	/**Lista con los �ltimos iBeacons detectados*/
	private ArrayList<iBeacon> detectediBeacons;

    public static final String MAC_BLE = "C2:D2:63:C5:70:B3";

	/**
	 * Constructor de la clase.
	 * @param iBeaconIdentifiers Lista con los identificadores de los iBeacons que se quieren detectar.
	 */
	public iBeaconDetection(ArrayList<UUID> iBeaconIdentifiers){
		this.iBeaconIdentifiers = iBeaconIdentifiers;
		this.scanner = BluetoothAdapter.getDefaultAdapter().getBluetoothLeScanner();
	}
	
	/**
	 * Devuelve una lista con todos los iBeacons detectados durante el �ltimo escaneo.
	 */
	public synchronized ArrayList<iBeacon> getDetectediBeacons(){
		return this.detectediBeacons;
	}
	
	/**
	 * Inicia el escaneo de iBeacons cercanos. Solo ser� necesario llamar a esta funci�n una vez, ya que a partir de 
	 * la primera invocaci�n escanear� autom�ticament cada pocos segundos.
	 * @param useiBeaconIdentifiers Si es true, solo se detectar�n los iBeacons cuyo UUID coincida con uno de los
	 * guardados en la variable iBeaconIdentifiers. Si es false, se detectar�n todos los iBeacons cercanos.
	 */
	public void startScan(boolean useiBeaconIdentifiers){
		//Se configura el escaner para usar la menor bater�a posible
		ScanSettings settings = new ScanSettings.Builder()
			.setScanMode(ScanSettings.SCAN_MODE_LOW_POWER)
			.build();
		
		ArrayList<ScanFilter> filters = new ArrayList<ScanFilter>();
		if(useiBeaconIdentifiers){
			//Se configura el escaner para solo buscar los iBeacons de la lista iBeaconIdentifiers.
			for (String mac : AccessPointData.loadRegisteredAccessPointAddresses()){
				ScanFilter filter = new ScanFilter.Builder()
					.setDeviceAddress(mac).build();
				filters.add(filter);
			}
		}
		
		//Se inicia el escaneo con los filtros indicados (o sin filtros, si useiBeaconIdentifiers == false)
		this.scanner.startScan(filters, settings, this);
	}
	
	/**
	 * Para el escaneo de iBeacons. 
	 */
	public void stopScan(){
		this.scanner.stopScan(this);
	}
	
	
	// Funciones de ScanCallback
	
	/*TODO: Dentro de ScanResult hay un BluetoothDevice, con el quiz�s se podr�a crear la conexi�n
	 * Bluetooth con el controlador de forma m�s sencilla que con la clase Bluetooth...*/
	@Override
	public void onScanResult (int callbackType, ScanResult result){
		List<ScanResult> list = new ArrayList<ScanResult>();
		list.add(result);
		this.onBatchScanResults(list);
	}
	
	@Override
	public void onBatchScanResults (List<ScanResult> results){
		ArrayList<iBeacon> ibeacons = new ArrayList<iBeacon>();
        String address = "";
		for(ScanResult result : results){
                address = result.getDevice().getAddress();
                // Esto esta puesto asi, para evitar problemas con otras macs, de momento.
                String name = result.getDevice().getName();
                int power = result.getRssi(); //TODO: �No ser�a mejor usar result.getScanRecord().getTxPowerLevel()?
                iBeacon ibeacon = new iBeacon(name, address, power);
                ibeacons.add(ibeacon);


		}


		//TODO: ordenar la lista por los que m�s potencia tengan.

        if (address.equals(MAC_BLE)) {
            this.detectediBeacons = ibeacons;

            ((iBeaconAdapter) AuthFragment.listiBeacons.getAdapter()).setItems(this.detectediBeacons);
        }
	}
	
	@Override
	public void onScanFailed (int errorCode){
		System.out.println("ERROR EN EL ESCANEO DE IBEACONS. CODIGO DE ERROR: " +errorCode);
	}
	
}
