package com.mecatronica.dbdevice.basic;

/**
 * Created by Javier on 17/11/14.
 * Interfaz que indica que funciones a parte de las generales de una conexion, ha de tener el
 * Bluetooth
 */
public interface IBluetooth extends IConnection {
}
