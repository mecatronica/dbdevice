package com.mecatronica.dbdevice.basic;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.ListView;

import com.mecatronica.dbdevice.adapters.iBeaconAdapter;
import com.mecatronica.dbdevice.fragments.AuthFragment;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Clase que se encarga basicamente de hacer el trabajo pesado en background para que no afecte
 * al hilo de la interfaz. La he hecho privada por que no creo que sea necesario una clase aparte para esto.
 * Si se cree necesario hacerlo, se cambiaria y no haria ningun problema.
 */
public class LoadBeacons extends AsyncTask<Void, Void, iBeaconAdapter> {

    private Context context;
    private ListView list;

    public LoadBeacons(Context context, ListView listiBeacons) {
        this.context = context;
        this.list = listiBeacons;
    }
    @Override
    protected iBeaconAdapter doInBackground(Void... params) {

        /**
         * Aqui tenemos que actualizar el adapter para listar los ibeacon
         * cada cierto tiempo
         */
        iBeaconDetection iBeacon = new iBeaconDetection(null);
        iBeacon.startScan(false);

        ArrayList<iBeacon> iBeacons = new ArrayList<>();

        iBeaconAdapter adapter = new iBeaconAdapter(context, iBeacons);

        TimerTask ttask = new TimerTask() {
            @Override
            public void run() {
                AuthFragment.updaterHandler.sendEmptyMessage(AuthFragment.UPDATE_LIST_IBEACONS);
            }
        };
        new Timer().schedule(ttask, 2000, 5000);

        return adapter;
    }

    @Override
    protected void onPostExecute(iBeaconAdapter iBeaconAdapter) {
        super.onPostExecute(iBeaconAdapter);
        list.setAdapter(iBeaconAdapter);
    }
}