package com.mecatronica.dbdevice.basic;

import java.util.ArrayList;

/**
 * Created by Javier on 16/11/14.
 * Esta clase representa un usuario del sistema. Contiene su nombre de usuario, asi como su pass
 * y el tipo de usuario que es y las salas a las que esta permitido entrar.
 */
public class User {

    /**
     * Nombre del usuario, identificador.
     */
    private String name;

    /**
     * Contraseña usada por el usuario para poder entrar a las salas.
     */
    private String pass;

    /**
     * El tipo de usuario indica si un usuario es administrador o un usuario normal y corriente.
     * En un futuro esto se puede amplicar con roles de usuario.
     */
    private UserType type;

    /**
     * Salas a las que esta permitido entrar el usuario en concreto.
     */
    private ArrayList<Room> allowedRooms;

    public User(String name, String pass, UserType type, ArrayList<Room> allowedRooms) {
        this.name = name;
        this.pass = pass;
        this.type = type;
        this.allowedRooms = allowedRooms;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public UserType getType() {
        return type;
    }

    public void setType(UserType type) {
        this.type = type;
    }
}

