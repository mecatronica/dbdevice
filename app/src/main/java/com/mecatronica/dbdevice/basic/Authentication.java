package com.mecatronica.dbdevice.basic;

/**
 * Created by Javier on 22/11/14.
 * Clase que se utiliza para enviar y recibir los datos al controlador.
 */
public class Authentication {

    /**
     * Envia una sala y un usuario al controlador para que este compruebe si el usuario tiene acceso
     * a la sala, y si la contraseña para acceder es correcta.
     * @param room sala a la que esta conectado.
     * @param user usuario que quiere entrar a la sala.
     */
    public static void sendAuth(Room room, User user) {

    }

    /**
     * Recibe un boolean si los credenciales de acceso a la sala son correctos, o false en caso
     * contrario.
     * @return true si puede entrar, false si no puede entrar.
     */
    public static boolean receive() {
        return false;
    }
}
