package com.mecatronica.dbdevice.basic;

/**
 * Created by Javier on 16/11/14.
 * Esta clase representa una sala, debera de tener un identificador unico, asi como un nombre que
 * sea legible por el usuario final.
 */
public class Room {
    /**
     * Identificador de la sala, tiene que ser unico.
     */
    private String uuid;

    /**
     * Nombre legible por el usuario final.
     */
    private String name;

    /**
     * Identifica en que region del recinto esta el punto de acceso.
     */
    private String major;

    /**
     * Usado para tener una mayor precision de la localizacion.
     */
    private String minor;

    /**
     * Llave inicial del dispositivo de autentificacion.
     */
    private String key;

    /**
     * Contraseña con la que se hara la primera autenticacion.
     */
    private String pass;

    public Room(String uuid, String name) {
        this.uuid = uuid;
        this.name = name;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getMinor() {
        return minor;
    }

    public void setMinor(String minor) {
        this.minor = minor;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    /**
     * Funcion que salvara el fichero de configuracion de la sala en disco.
     */
    public void save() {
    }

    /**
     * Funcion que leera el fichero de configuracion de la sala en disco.
     */
    public void read() {
    }
}
