package com.mecatronica.dbdevice.basic;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.util.Log;

import com.mecatronica.common.NetworkObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

/**
 * Created by Javier on 4/12/14.
 */
public class Bluetooth implements IBluetooth {
    private BluetoothAdapter bluetoothAdapter;
    private BluetoothDevice device;
    private BluetoothSocket socket;
    private Activity activity;

    private static final UUID MY_UUID = UUID.fromString("fa87c0d0-afac-11de-8a39-0800200c9a66");

    public Bluetooth(Activity activity) {
        this.activity = activity;
    }

    @Override
    public boolean connect(String uuid) {
        boolean connected = true;
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        // Si no tenemos Bluetooth en el movil no nos podemos conectar...
        if(bluetoothAdapter == null)
            return false;

        // Si no tenemos Bluetooth activado, les pedimos que lo activen.
        if(!bluetoothAdapter.isEnabled()){
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            activity.startActivityForResult(enableBtIntent, 1);
        }

        device = bluetoothAdapter.getRemoteDevice(uuid);

        if (device == null)
            return false;

        try {
            socket = device.createRfcommSocketToServiceRecord(MY_UUID);
        } catch (IOException e) {
            Log.e("Bluetooth", "Socket Type: secure create() failed", e);
        }

        return true;
    }

    @Override
    public void send(NetworkObject data) {
        try {
            OutputStream outStream = socket.getOutputStream();
            outStream.write(data.convertToBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public NetworkObject receive() {
        //TODO: No se que tamaño poner aqui, y en Java no se puede calcular el tamaño de un objeto como en C
        //TODO: Si pongo buffer = null, ¿no habra problemas con la memoria?
        byte [] buffer = new byte[1024*1024];
        try {
            InputStream inputStream = socket.getInputStream();
            inputStream.read(buffer);
        }
        catch (IOException e) { }

        return NetworkObject.bytesToObject(buffer);
    }

    @Override
    public boolean close() {
        boolean close = false;
        try {
            socket.close();
            close = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return close;
    }
}
