package com.mecatronica.dbdevice.basic;

import com.mecatronica.common.NetworkObject;

/**
 * Created by Javier on 17/11/14.
 * Funciones basicas que ha de tener cualquier conexion, que utilicemos.
 */
public interface IConnection {

    boolean connect (String identifier);

    void send (NetworkObject data);

    NetworkObject receive ();

    boolean close();
}
